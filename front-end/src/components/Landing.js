import React, { Component, useEffect, useState }  from 'react';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import Avatar from '@material-ui/core/Avatar';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ListAltIcon from '@material-ui/icons/ListAlt';
import Card from '@material-ui/core/Card';
import RecReportCard from './RecReportCard';
import AllReports from './AllReports';
import TotalReportsCard from './TotalReportsCard';
import PassCard from './PassCard';
import FailCard from './FailCard';
import FailPercentGraphCard from './FailPercentGraphCard';
import NumFailuresGraphCard from './NumFailuresGraphCard';
import Typography from '@material-ui/core/Typography';
import FadeIn from 'react-fade-in';
import Modal from '@material-ui/core/Modal';
import { serverIP } from './ComponentConstants'

function Landing(props){

  const [page, setPage] = useState(0);
  const [reportOpen, setReportOpen] = useState(0);
  const [reportNameToOpen, setReportNameToOpen] = useState(0);

  useEffect(()=>{
    props.getReportRecommendations(props.user);
    props.getAllReports();
    setPage("recommendations");
    setReportOpen(false);
  }, [])

  const openReport = (reportName) => {
    setReportOpen(true);
    setReportNameToOpen(reportName)
  }


  const calculatePasses = (allReports)=>{
    if (allReports !== undefined){
    return allReports.reduce((accumulator, currentValue) => {
    return accumulator + parseInt(currentValue["Pass"])}, 0)
  } else {
    return 0
  }
  }

  const calculateFails = (allReports)=>{
    if (allReports !== undefined){
    return allReports.reduce((accumulator, currentValue) => {
    return accumulator + parseInt(currentValue["Fail"])}, 0)
  } else {
    return 0
  }
  }

  const recommendedReportList = ()=>{
    if (props.recommendedReports !== undefined){
      return props.recommendedReports.map((e)=><div><Button>{e}</Button><br/></div>)
    } else {
      return <p>None</p>
    }
  }

  const allReportsList = ()=>{
    if (props.allReports !== undefined){
      return props.allReports.map((e)=><div><Button>{e}</Button><br/></div>)
    } else {
      return <CircularProgress />
    }
  }

  const buildRecReportCards = () => {
    if (props.recommendedReports !== undefined){
      return props.recommendedReports.map((e)=>{
        return (
          <RecReportCard
            name={e["Report Name"]}
            passes={e["Pass"]}
            fails={e["Fail"]}
            suite={e["Suite Name"]}
            date= {e["Time"]}
            open= {openReport.bind(this)}/>
        )
      })
    }
  }

  const getNumRecomendations = () => {
    if (props.recommendedReports !== undefined){
      return props.recommendedReports.length
    } else {
      return 0
    }
  }

  const showSelectedPage = () => {
    if (page === "recommendations"){
      return (
        <div className='main-panel'>
        <Typography variant='h4' style={{paddingTop: 25, color: '#777777', float: 'left', paddingLeft: 20}}>Recommendations ({getNumRecomendations()})</Typography>
        <FadeIn>
        <Grid container>
         {buildRecReportCards()}
        </Grid>
        </FadeIn>
        </div>
      )
    }
    if (page === "allReports"){
      return (
        <div>
          <AllReports
            reports={props.allReports}
            open={openReport.bind(this)}/>
        </div>
      )
    }
    if (page === "overview"){
      return (
        <div className='main-panel'>
        <FadeIn>
        <Grid container>
         <TotalReportsCard
           numReports={props.allReports.length}/>
         <PassCard
           numReports={calculatePasses(props.allReports)}/>
         <FailCard
           numReports={calculateFails(props.allReports)}/>
         <FailPercentGraphCard
           reports={props.allReports}
           open={openReport.bind(this)}/>
         <NumFailuresGraphCard
           reports={props.allReports}
           open={openReport.bind(this)}/>
        </Grid>
        </FadeIn>
        </div>
      )
    }
  }

  return(
    <div>
    <Modal
      open={reportOpen}
      onClose={()=>{setReportOpen(false)}}>
      <Paper className='paper-modal'>
      <iframe src={'http://'+serverIP+'/view_report/'+props.user+'/'+reportNameToOpen}
        style={{height: '100%', width: '100%'}}></iframe></Paper>
    </Modal>
      <div className='left-panel'>
      <div className='avatar-name'>
        <AccountCircleIcon style={{ fontSize: 50 }} />
        <span className='user-name'>{props.user}</span>
      </div>
      <hr/>
      <MenuItem
      selected={page === "overview"}
      onClick={()=> {setPage("overview")}}>
        <ListItemIcon>
          <ShowChartIcon />
        </ListItemIcon>
        OVERVIEW
      </MenuItem>
      <MenuItem
        selected={page === "recommendations"}
        onClick={()=> {setPage("recommendations")}}>
      <ListItemIcon>
        <FavoriteIcon />
      </ListItemIcon>RECOMMENDATIONS
      </MenuItem>
      <MenuItem
        selected={page === "allReports"}
        onClick={()=> {setPage("allReports")}}>
      <ListItemIcon>
        <ListAltIcon />
      </ListItemIcon>ALL REPORTS
      </MenuItem>
      </div>
      {showSelectedPage()}
    </div>
  )

}

export default Landing;
