import React, { Component }  from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import FadeIn from 'react-fade-in';
import IconButton from '@material-ui/core/IconButton';
import ListAltIcon from '@material-ui/icons/ListAlt';

function AllReports(props){

  const getHumanDate = (dateString)=>{
    var moment = require('moment');
    return moment(dateString, "YYYYMMDD HH:mm:ss").format("MMMM Do YYYY, HH:mm:ss")
  }

  return(
    <div className='main-panel all-reports-panel'>
    <FadeIn>
  <Table>
  <TableHead>
          <TableRow>
            <TableCell>Report Name</TableCell>
            <TableCell align="right">Suite Name</TableCell>
            <TableCell align="right">Pass</TableCell>
            <TableCell align="right">Fail</TableCell>
            <TableCell align="right">Time</TableCell>
            <TableCell align="right">View</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.reports.map(row => (
            <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {row["Report Name"]}
              </TableCell>
              <TableCell align="right">{row["Suite Name"]}</TableCell>
              <TableCell align="right">{row["Pass"]}</TableCell>
              <TableCell align="right">{row["Fail"]}</TableCell>
              <TableCell align="right">{getHumanDate(row["Time"])}</TableCell>
              <TableCell align="right">
                <IconButton
                  onClick={()=>{props.open(row["Report Name"])}}>
                  <ListAltIcon fontSize="medium" />
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
  </Table>
  </FadeIn>
  </div>
  )

}

export default AllReports;
