import React, { Component }  from 'react';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import ErrorIcon from '@material-ui/icons/Error';

function FailCard(props){
  return (
    <Grid item xs={4}>
    <Card className='report-card'>
    <Grid container>
      <Grid item xs={2} style={{display: 'flex', alignItems: 'center'}}>
        <ErrorIcon style={{ fontSize: 80, color: '#D8000C', margin: '0 auto', display: 'block' }}/>
      </Grid>
      <Grid item xs={10}>
      <span style={{fontSize: '2rem', color: '#D8000C'}}>Total Fails</span>
      <br/>
      <span style={{fontSize: '3rem', fontWeight: 600, color: '#111'}}>{props.numReports}</span>
      </Grid>
    </Grid>
    </Card>
    </Grid>
  )
}

export default FailCard;
