import React, { Component }  from 'react';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import {Line} from 'react-chartjs-2';

function FailPercentGraphCard(props){

  const compareDate = ( a, b ) => {
  if ( a["Time"] < b["Time"] ){
    return -1;
  }
  if ( a["Time"] > b["Time"] ){
    return 1;
  }
  return 0;
}

  var LineChart = require("react-chartjs").Line;
  var moment = require('moment');

  var sortedReports = props.reports.sort(compareDate);

  const getHumanDate = (dateString)=>{
    return moment(dateString, "YYYYMMDD HH:mm:ss").toDate()
  }

  var labels = sortedReports.map((e)=> getHumanDate(e["Time"]))
  var dataSet = sortedReports.map((e)=> (parseInt(e["Pass"]) / (parseInt(e["Pass"]) + parseInt(e["Fail"])))*100)

  var options = {responsive: true,
                 maintainAspectRatio: false,
                 fill: false,
                 title: {
                   display: true,
                   text: 'Test Pass %',
                   fontSize: 20
                 },
                 scales: {
              xAxes: [{
                  type: 'time',
                  time: {
                      unit: 'day'
                  }
              }],
              yAxis: [{
                scaleLabel: {
                  display: true,
                  labelString: "Percentage"
                }
              }]
          }}

  var data = {labels: labels,
              datasets: [
                {label: "Test pass %",
                 fill: false,
                 lineTension: 0,
                 borderColor: "#99badd",
          			 pointStrokeColor: "#fff",
          			 pointHighlightStroke: "rgba(153, 186, 221,1)",
                 data: dataSet}
              ]}
  return(
    <Grid item xs={12}>
      <Card className='graph-card'>
        <div style={{height: '100%', width:'100%'}}>
        <Line data={data} options={options} onElementsClick={elems => {
          if (elems.length > 0){
            props.open(sortedReports[elems[0]._index]["Report Name"])
          }
        }}/>
        </div>
      </Card>
    </Grid>
  )

}

export default FailPercentGraphCard;
