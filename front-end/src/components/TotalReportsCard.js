import React, { Component }  from 'react';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import ListAltIcon from '@material-ui/icons/ListAlt';

function TotalReportsCard(props){
  return (
    <Grid item xs={4}>
    <Card className='report-card'>
    <Grid container>
      <Grid item xs={2} style={{display: 'flex', alignItems: 'center'}}>
        <ListAltIcon style={{ fontSize: 80, color: 'purple', margin: '0 auto', display: 'block' }}/>
      </Grid>
      <Grid item xs={10}>
      <span style={{fontSize: '2rem', color: 'purple'}}>Total Reports</span>
      <br/>
      <span style={{fontSize: '3rem', fontWeight: 600, color: '#111'}}>{props.numReports}</span>
      </Grid>
    </Grid>
    </Card>
    </Grid>
  )
}

export default TotalReportsCard;
