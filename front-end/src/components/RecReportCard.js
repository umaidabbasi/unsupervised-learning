import React, { Component }  from 'react';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import ListAltIcon from '@material-ui/icons/ListAlt';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import Button from '@material-ui/core/Button';

function RecReportCard(props){

  const getHumanDate = (dateString)=>{
    var moment = require('moment');
    return moment(dateString, "YYYYMMDD HH:mm:ss").format("MMMM Do YYYY, HH:mm:ss")
  }

  return (
    <Grid item xs={4}>
      <Card className='report-card'>
      <span className='rec-report-text'> RECOMMENDED REPORT </span>
      <br/>
      <ListAltIcon style={{ fontSize: 80 }} />
      <br/>
        <span className='suite-name'>{props.suite}</span>
        <br/>
        <span className='pass-fail-text'>{props.name}</span>
        <p>{getHumanDate(props.date)}</p>
        <Grid container>
          <Grid item xs={6}>
            <CheckCircleIcon style={{ fontSize: 50, color: '#4BCA81' }}/>
            <br/>
            <span className='pass-fail-text'>{props.passes} Passed</span>
          </Grid>
          <Grid item xs={6}>
            <ErrorIcon style={{ fontSize: 50, color: '#D8000C' }}/>
            <br/>
            <span className='pass-fail-text'>{props.fails} Failed</span>
          </Grid>
        </Grid>
        <Button
          fullWidth
          variant="contained"
          color="dark"
          className='button'
          onClick={()=>{props.open(props.name)}}>View Report</Button>
      </Card>
    </Grid>
  );
}

export default RecReportCard;
