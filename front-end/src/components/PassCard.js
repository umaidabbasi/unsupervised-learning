import React, { Component }  from 'react';
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';

function PassCard(props){
  return (
    <Grid item xs={4}>
    <Card className='report-card'>
    <Grid container>
      <Grid item xs={2} style={{display: 'flex', alignItems: 'center'}}>
        <CheckCircleIcon style={{ fontSize: 80, color: '#4BCA81', margin: '0 auto', display: 'block' }}/>
      </Grid>
      <Grid item xs={10}>
      <span style={{fontSize: '2rem', color: '#4BCA81'}}>Total Passes</span>
      <br/>
      <span style={{fontSize: '3rem', fontWeight: 600, color: '#111'}}>{props.numReports}</span>
      </Grid>
    </Grid>
    </Card>
    </Grid>
  )
}

export default PassCard;
