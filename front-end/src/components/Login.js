import React, { Component }  from 'react';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';

function Login(props) {

  const getUsernameItems = () => {
    if ( props.users !== undefined && Object.keys(props.users).length !== 0) {
      let menuItems

      return(
        props.users.map(e =>
        <MenuItem value={e}>{e}</MenuItem>)
      )
    } else {
      console.log("empty")
    }
  }

  return(
    <div className='paper'>
    <Avatar className='avatar'>
      <LockOutlinedIcon />
    </Avatar>
    <Typography component="h1" variant="h5">
      Sign in
    </Typography>
    <FormControl className='login-form'>
      <InputLabel htmlFor="username-helper">Username</InputLabel>
      <Select
        className='username-select'
        fullWidth
        value = {props.username}
        onChange={props.handleChange}
        inputProps={{
          name: 'username',
          id: 'username-helper',
        }}
      >
      {getUsernameItems()}
      </Select>
      <Button
        fullWidth
        variant="contained"
        color="primary"
        className='button'
        onClick= {props.setUser}
      >
        Sign In
      </Button>
    </FormControl>
    </div>
  )
}

export default Login;
