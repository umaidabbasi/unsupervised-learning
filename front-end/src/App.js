import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { connect } from 'react-redux';
import { simpleAction, getReportRecommendations, getAllReports } from './actions/simpleAction';
import Input from '@material-ui/core/Input';
import Login from './components/Login'
import Landing from './components/Landing'

const mapStateToProps = state => ({
...state
})

const mapDispatchToProps = dispatch => ({
  simpleAction: (username) => dispatch(simpleAction(username)),
  getReportRecommendations: (username) => dispatch(getReportRecommendations(username)),
  getAllReports: () => dispatch(getAllReports())
})

class App extends Component {
  constructor(props){
    super()
    this.state = {}
  }

  componentDidMount() {
    this.simpleAction()
  }

  simpleAction = (event) => {
 this.props.simpleAction();
}

handleChange = (e) => {
  this.setState({username: e.target.value})
}

setUser = () => {
  this.setState({loggedInUser: this.state.username})
}

renderPage = () => {
  if (this.state.loggedInUser !== undefined){
    return <Landing
      user={this.state.loggedInUser}
      getReportRecommendations = {this.props.getReportRecommendations.bind(this)}
      recommendedReports = {this.props.simpleReducer.recommendedReports}
      getAllReports = {this.props.getAllReports.bind(this)}
      allReports = {this.props.simpleReducer.allReports} />
  } else {
    return <Login
    users={this.props.simpleReducer.result}
    handleChange = {this.handleChange.bind(this)}
    username ={this.state.username}
    setUser = {this.setUser.bind(this)}/>
  }
}

  render(){
  return (
    <div className="App">

    <CssBaseline />
      {this.renderPage()}

    </div>
  )};
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
