import { serverIP } from '../components/ComponentConstants'

export const simpleAction = () => dispatch => {
  fetch('http://'+serverIP+'/users')
  .then( response => response.json())
  .then(json => dispatch({
  type: 'SIMPLE_ACTION',
  payload: json
}))
}

export const getReportRecommendations = (username) => dispatch => {
  fetch('http://'+serverIP+'/recommendations/'+username)
  .then( response => response.json())
  .then(json => dispatch({
  type: 'FETCH_RECOMMENDED_REPORTS',
  payload: json
}))
}

export const getAllReports = () => dispatch => {
  fetch('http://'+serverIP+'/reports')
  .then( response => response.json())
  .then(json => dispatch({
  type: 'FETCH_ALL_REPORTS',
  payload: json
}))
}
