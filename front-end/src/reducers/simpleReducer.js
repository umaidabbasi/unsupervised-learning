export default (state = {}, action) => {
 switch (action.type) {
  case 'SIMPLE_ACTION':
   return {
    ...state, result: action.payload
  }
  case 'FETCH_RECOMMENDED_REPORTS':
   return {
    ...state, recommendedReports: action.payload
   }
  case 'FETCH_ALL_REPORTS':
   return {
    ...state, allReports: action.payload
  }
  default:
   return state
 }
}
