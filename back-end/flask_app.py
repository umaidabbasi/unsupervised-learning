from flask import Flask, request, render_template, jsonify, send_file, make_response
import os
import back_end as be
import report_parse as rp
from flask_cors import CORS
from flask import g
import report_parse_selenium as rps

# Create the application instance

app = Flask(__name__, template_folder="templates")
CORS(app)

@app.route('/login/<user>', methods=['GET'])
def login(user):
    if request.method == 'GET':
        app.logger.info(be.find_user(user))
        f_user = be.find_user(user)
        return jsonify(f_user)

    
@app.route('/users', methods=['GET'])
def users():
    if request.method == 'GET':
        users = be.get_users()
        app.logger.info(users)
        return jsonify(users)


@app.route('/reports', methods=['GET'])
def reports():
    if request.method == 'GET':
        reportsList = []
        for report_name in test_reports.keys():
            content = {
                "Report Name": report_name,
                "Suite Name": test_reports[report_name]["suite_name"],
                "Time": test_reports[report_name]["time"],
                "Pass": str(test_reports[report_name]["pass"]),
                "Fail": str(test_reports[report_name]["fail"]),
            }
            reportsList.append(content)

    return jsonify(reportsList)


@app.route('/view_report/<user>/<name>', methods=['GET'])
def report(user, name):
    try:
        reports_dir = be.view_report(name)
        be.add_user_report_count(user, name)
        return send_file(reports_dir)
    except Exception:
        content = {name: 'not found'}
        return make_response(jsonify(content), 404)


@app.route('/recommendations/<user>', methods=['GET'])
def recommendations(user):
    if request.method == 'GET':
        reportsList = []
        reports_ml = be.ml(user)
        for report_name in test_reports.keys():
            for report_ml in reports_ml:
                if report_name == report_ml:
                    content = {
                "Report Name": report_name,
                "Suite Name": test_reports[report_name]["suite_name"],
                "Time": test_reports[report_name]["time"],
                "Pass": str(test_reports[report_name]["pass"]),
                "Fail": str(test_reports[report_name]["fail"]),
                    }
                    reportsList.append(content)
    return make_response(jsonify(reportsList), 200)


@app.route('/report_details/<report>', methods=['GET'])
def report_details(report):
    global report_path
    content = {
        "Suite Name":test_reports[report]["suite_name"],
        "Time":test_reports[report]["time"],
        "Pass":str(test_reports[report]["pass"]),
        "Fail": str(test_reports[report]["fail"]),
    }

    return make_response(jsonify(content), 200)


if __name__ == '__main__':
    # create user data, path for user csv and reports
    be.populate_reports_csv()
    csv = os.path.realpath('./back-end/users.csv')
    report_path = os.path.realpath('./back-end/reports')
    test_reports = rps.parse_reports_html_debug(report_path, 0, 0)

    app.run(debug=True)

