import pandas as pd
import os
import csv
import numpy as np
import names
import recommender as rc
import report_parse_selenium as rps

csv = os.path.realpath('./back-end/users.csv')
report_path = os.path.realpath('./back-end/reports')


def find_user(user):
    data = pd.read_csv(csv, sep=',',  index_col=0)
    user_list = data.index
    for f_user in user_list:
        if f_user.casefold() == user.casefold():
            return f_user


def get_users():
    data = pd.read_csv(csv, sep=',',  index_col=0)
    f_user = (list(data.index))
    return f_user


def add_user(user):
    with open(csv, 'a') as csv_file:
        csv_file.write(user)
    return user


def get_reports():
    reports_dir = report_path
    files = os.listdir(reports_dir)
    return files


def view_report(name):
    reports_dir = os.listdir(report_path)
    for file in reports_dir:
        if file == name:
            return report_path + '/' + file



def add_user_report_count(user, name):
    u = find_user(user)
    data = pd.read_csv(csv, sep=',', index_col=0)
    report_count = data.get_value(u, name)
    updated_report_count = report_count + 1
    data.loc[u, name] = updated_report_count
    data.to_csv(csv, sep=',')



def populate_users_csv():
    user_index = []
    while(len(user_index) < 50):
        name = names.get_first_name()
        if name not in user_index:
            user_index.append(name)
    return user_index


def populate_reports_csv():
    report_list = get_reports()
    data_col = []
    user_index = populate_users_csv()
    for col in report_list:
        data_col.append(col)
    rows, cols = (len(user_index), len(data_col))
    twod_array = np.stack([np.random.choice([0,0,0,0,0,0,0,0,0,1], cols, replace=True) for _ in range(rows)])
    dfObj = pd.DataFrame(twod_array, columns=data_col, index=user_index)
    dfObj.to_csv(csv, sep=',')

def ml(user):
    data, model = rc.generate_model(csv)
    return rc.recommendation(data, model, [user], 5)




















