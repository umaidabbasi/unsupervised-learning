import os
import re
import json

#Runs on boot, parses report information into panda dataframe which is written to a dictionary of reportname:report_values
def parse_reports(reports_dir):
    files = os.listdir(reports_dir)

    reports = {}

    for report in files:
        if re.search('^.*\.html$', report):
            stats = []
            time = 0

            file = open(os.path.join(reports_dir, report), "r")
            for line in file:
                stats_match = re.match('^window\.output\[\"stats\"\] = (.*);', line)
                time_match = re.match('^window\.output\[\"baseMillis\"\] = (\d+);', line)
                if stats_match is not None and len(stats) == 0:
                    stats = json.loads(stats_match.groups()[0])

                if time_match is not None:
                    time = time_match.groups()[0]


            file.close()

            #Write parsed data to dictionary
            report_dict = {}
            report_dict["suite_name"] = stats[0][-1]["label"]
            report_dict["time"] = time
            report_dict["critical_pass"] = stats[0][0]["pass"]
            report_dict["critical_fail"] = stats[0][0]["fail"]
            report_dict["pass"] = stats[0][1]["pass"]
            report_dict["fail"] = stats[0][1]["fail"]
            reports[report] = report_dict

    return reports
"""
test_reports = parse_reports(os.path.realpath('./back-end/reports'))
print("Suite Name: " + test_reports["cats_log_20190426-093757_nuage5.3.3_vspk5.3.3.html"]["suite_name"])
print("Time: " + test_reports["cats_log_20190426-093757_nuage5.3.3_vspk5.3.3.html"]["time"])
num_pass = test_reports["cats_log_20190426-093757_nuage5.3.3_vspk5.3.3.html"]["pass"]
total = num_pass + test_reports["cats_log_20190426-093757_nuage5.3.3_vspk5.3.3.html"]["fail"]
print("Percent passed: " + str(num_pass*100/total))
"""