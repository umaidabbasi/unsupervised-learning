import numpy as np
import pandas as pd
from lightfm import LightFM
from lightfm.data import Dataset

def generate_model(csvpath):
  #Read CSV path which should contain pandas dataframe of users, reports, interactions -> CSV
  user_df = pd.read_csv(csvpath, index_col=0)

  #Create dataset and internal indexes for dataset
  report_data = Dataset()
  report_data.fit(list(user_df.index), list(user_df.columns))

  interactionTuples = []
  for user in list(user_df.index):
    for report in list(user_df.columns):
      interactionTuples.append(tuple((user, report, user_df[report][user])))
  (interactions, weights) = report_data.build_interactions(interactionTuples)

  # Instantiate and train the Weighted Approximate-Rank Pairwise loss model
  report_model = LightFM(loss='warp')
  report_model.fit(interactions)

  return user_df, report_model


def recommendation(df, model, user_ids, num_reqs):
  num_items = len(df.columns)

  for user in user_ids:
    if not str(user).isdigit():
      user = int(list(df.index).index(user))

    #Generate all loss model ratings for all reports for the use
    scores = model.predict(user, np.arange(num_items))

    #Return the top reports by report name, in ranked order
    top = (np.argsort(-scores)[-num_reqs:][::-1])
    top_reports = []
    for result in top:
      top_reports.append(list(df.columns)[result])

    return top_reports

'''
#Example Usage:
data, model = generate_model('users.csv')
print(recommendation(data, model, ["Ian"], 5))
print(recommendation(data, model, ["Robert"], 5))
print(recommendation(data, model, ["Patricia"], 5))
print(recommendation(data, model, ["Carmen"], 5))
'''

