##### report_parse_selenium.py ####################################################################################################
###
### This file is used to process incoming Robot reports and generate summary reports to be used by the
### results predicting application. Several methods and output types are available, and the output contract
### is described below.
###
### REPORT FORMATS:
###    REPORT_URL: string, key to report entries. URL filename of Robot report parsed
###
###    version 0 (legacy):
###        { REPORT_URL : report_dict}
###
###    version 1 (DOM-based):
###        { REPORT_URL : {"summary": summary_dict, "suites": [per_suite_list], "tests": [per_test_list]}}
###
### SUB-REPORT FORMATS:
###    report_dict:
###        { 
###        "suite_name"      : string, display name of first suite in report
###        "time"            : string, elapsed time of first suite in report. format HH:MM:SS
###        "critical_pass"   : integer, count of all passed critical tests
###        "critical_fail"   : integer, count of all failed critical tests
###        "pass"            : integer, count of all passsed tests
###        "fail"            : integer, count of all failed tests
###        }
###
###    summary_dict:
###        { 
###        "crit_pass_count" : integer, count of all passed critical tests for this report
###        "crit_fail_count" : integer, count of all failed critical tests for this report
###        "all_pass_count"  : integer, count of all passsed tests for this report
###        "all_fail_count"  : integer, count of all failed tests for this report
###        "crit_duration"   : string, elapsed time of all critical tests in this report. format HH:MM:SS
###        "all_duration"    : string, elapsed time of all tests in this report. format HH:MM:SS
###        "all_gen_time"    : string, timestamp of generation time of this report. format 'YYYYMMDD HH:MM:SS UTC[+-]HH:MM'
###        }
###
###    per_suite_list:
###        { 
###        "suite_name_short"        : string, id of suite as it appears in the DOM-rendered Robot report
###        "suite_name_long"         : string, name of suite as it appears to end-user
###        "parent_suite_name_short" : string, id of parent suite as it appears in the DOM-rendered Robot report. "" if no parent
###        "parent_suite_name_long"  : string, name of parent suite as it appears to end-user. "" if no parent
###        "suite_pass_count"        : integer, count of all passsed tests for this suite
###        "suite_fail_count"        : integer, count of all failed tests for this suite
###        "suite_crit_pass_count"   : integer, count of critical tests passed in this suite
###        "suite_crit_fail_count"   : integer, count of critical tests passed in this suite
###        "suite_duration"          : string, elapsed time of all tests in this suite, plus setup and teardown. format HH:MM:SS
###        "suite_source"            : string, originating URL for this test. may be from a filesystem or webpage
###        "suite_start_time"        : string, timecode for start of this suite. format 'YYYYMMDD HH:MM:SS.mmm'
###        "suite_end_time"          : string, timecode for end of this suite. format 'YYYYMMDD HH:MM:SS.mmm'
###        "suite_elapsed_time"      : string, timecode for duration of this suite. format HH:MM:SS.mmm
###        "suite_keywords"          : [keyword], any keywords unique to this suite
###        }
###
###    per_test_list:
###        { 
###        "test_name"               : string, name of test as it appears to end-user
###        "test_name_long"          : string, long name of test as it appears to end-user
###        "test_name_short"         : string, id of test as it appears in the DOM-rendered Robot report
###        "parent_test_name_short"  : string, id of parent test as it appears in the DOM-rendered Robot report. "" if no parent
###        "parent_test_name_long"   : string, name of parent test as it appears to end-user. "" if no parent
###        "parent_suite_name_short" : string, id of parent suite as it appears in the DOM-rendered Robot report. "" if no parent
###        "parent_suite_name_long"  : string, name of parent suite as it appears to end-user. "" if no parent
###        "test_passed"             : integer, indicates pass (0) fail (1), not implemented (-1)
###        "test_is_critical"        : integer, indicates non-critical (0), critical (1), not implemented (-1)
###        "test_start_time"         : string, timecode for start of this test. format HH:MM:SS.mmm
###        "test_end_time"           : string, timecode for end of this test. format HH:MM:SS.mmm
###        "test_elapsed_time"       : string, timecode for duration of this test. format HH:MM:SS.mmm
###        "test_keywords"           : [keyword], any keywords unique to this test
###        }
###
###    keyword:
###        { 
###        "keyword_name"         : string, name of keyword as it appears to end-user
###        "keyword_type"         : string, type of keyword ('var', 'for', 'keyword', etc.)
###        "keyword_start_time"   : string, timecode for start of this keyword. format HH:MM:SS.mmm
###        "keyword_end_time"     : string, timecode for end of this keyword. format HH:MM:SS.mmm
###        "keyword_elapsed_time" : string, timecode for duration of this keyword. format HH:MM:SS.mmm
###        "status"               : integer, indicates pass (0), fail (1), not implemented (-1)
###        "contents"             : string, message within keyword populated for end-user
###        "child_keywords"       : [keyword], any child keywords of this keyword
###        }
###
###    IS MY FIELD IMPLEMENTED? A guide by type (as indicated above). If you receive this value, the field is not yet usable.
###        integer   : -1
###        string    : "???"
###        keyword   : {}
###        [keyword] : []
###
###    IS MY FIELD PARSED CORRECTLY? A guide by type (as indicated above). If you receive this value, the field was read improperly.
###        integer   : -2
###        string    : "unknown"
###        keyword   : {}
###        [keyword] : []
###
###################################################################################################################################

import os
import re
import json

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from webdriverdownloader import GeckoDriverDownloader

def parse_reports_html_debug(reports_dir, outputMode, isDebugMode):
    OUTPUT_MODE_MAX = 1
    if (outputMode < 0) or (outputMode > OUTPUT_MODE_MAX):
        print("ERROR: Invalid report version requested. Please choose a version between 0 and " + OUTPUT_MODE_MAX + ". Setting report version to 0.")
        outputMode = 0

    files = os.listdir(reports_dir)
    reports = {}

    #Install Gecko Webdriver if necessary
    try:
        gdd = GeckoDriverDownloader()
        gdd.download_and_install()
        geckodriverPath = "../geckodriver"
    except:
        if isDebugMode:
            print("ERROR: failed to install geckodriver. application may fail to parse reports and/or end unexpectedly")

    options = Options()
    options.headless = True
    #browser = webdriver.Firefox(executable_path=geckodriverPath, firefox_options=options) #preferably, implement this way to avoid requiring the user to install geckodriver.
    browser = webdriver.Firefox(firefox_options=options)

    for report_url in files:
        if re.search('^.*.html$', report_url):
            report_dict = {}
            summary_dict = {}
            per_suite_list = []
            per_test_list = []
        
            stats = []
            time = 0

            numSuites = 0
            numTests = 0
            numSuccesses = 0
            numSuccessesCrit = 0
            numFails = 0
            numFailsCrit = 0
            allTestsTime = ""
            allGenTime = ""
            critTestsTime = ""
            firstSuiteTime = ""
            firstSuiteName = "" #consider deprecating after support is added for multiple suites

            browser.get("file://" + os.path.join(reports_dir, report_url))
            #innerHTML = browser.execute_script("return document.body.innerHTML") #potentially cleaner method avoiding browser use but requires lxml
            #===GET SUMMARY INFO=====================================================================================
            numSuites = 1 #this should be changed later to reflect possibility of multiple suites
            genSumElem = None
            try:
                genSumElem = browser.find_element_by_id("generated")
            except:
                if isDebugMode:
                    print("ERROR: report generated time could not be parsed")
            if genSumElem != None:
                allGenTime = genSumElem.text
                reducedGenTime = allGenTime.split("\n")
                if len(reducedGenTime) >= 3:
                    allGenTime = reducedGenTime[1]
            else:
                allGenTime = "unknown"
            
            elems = browser.find_elements_by_id("total-stats")
            if len(elems) > 0:
                childElems = elems[0].find_elements_by_class_name("row-1") #all test results
                durationElems = childElems
                if len(childElems) > 0:
                    durationElems = childElems[0].find_elements_by_class_name("stats-col-elapsed")
                    childElems = childElems[0].find_elements_by_class_name("stats-col-stat")
                elif isDebugMode:
                    print("no element of class 'row-1'")
                if len(childElems) >= 3:
                    numSuccesses = int(childElems[1].text)
                    numFails = int(childElems[2].text)
                elif isDebugMode:
                    print("ERROR: row-1 (all test results) had fewer than 3 child elements")
                if len(durationElems) > 0:
                    allTestsTime = str(durationElems[0].text)
                elif isDebugMode:
                    print("ERROR: row-1 (all test resutls) had no 'stats-col-elapsed'-class child elements")
                
                childElems = elems[0].find_elements_by_class_name("row-0") #critical test results
                if len(childElems) > 0:
                    childElems = childElems[0].find_elements_by_class_name("stats-col-stat")
                elif isDebugMode:
                    print("no element of class 'row-0'")
                if len(childElems) >= 3:
                    numSuccessesCrit = int(childElems[1].text)
                    numFailsCrit = int(childElems[2].text)
                elif isDebugMode:
                    print("ERROR: row-0 (crit test results) had fewer than 3 child elements")
            
            elif isDebugMode:
                print("ERROR: no elements with id 'total-stats'")
            #===GET LEGACY SUITE INFO=================================================================================
            elems = browser.find_elements_by_id("suite-stats")
            if len(elems) > 0:
                childElems = elems[0].find_elements_by_class_name("stat-name") #names of suites
                durationElems = elems[0].find_elements_by_class_name("stats-col-elapsed") #durations of suites
                if len(childElems) > 0:
                    firstSuiteName = str(childElems[0].text)
                elif isDebugMode:
                    print("no element of class 'stat-name'")
                if len(durationElems) > 0:
                    firstSuiteTime = str(durationElems[1].text)
                elif isDebugMode:
                    print("no element of class 'stats-col-elapsed'")
            elif isDebugMode:
                print("ERROR: no elements with id 'suite-stats'")
            if isDebugMode and (outputMode==0):
                print("for test " + str(report_url) + " with s1 name " + str(firstSuiteName) +
                    " passed " + str(numSuccesses) + " (" + str(numSuccessesCrit) + " crit)" + 
                    " failed " + str(numFails) + " (" + str(numFailsCrit) + " crit)" + 
                    " dur'n " + str(allTestsTime) + " total dur'n " + str(firstSuiteTime)
                    )
            #===GET DOM-BASED PET-SUITE AND PER-TEST INFO==============================================================
            suiteCounter = 1
            testCounter = 1
            
            elems = browser.find_elements_by_class_name("suite") #get all suite elements, including those which are children of other suites
            
            for suite in elems:
                suite_dict = {}
                #get element id
                suiteNameShort = suite.get_attribute('id')
                parentSuiteNameShort = ""
                #process per-suite name and parent name
                if suiteNameShort == None:
                    suiteNameShort = ""
                    #process parent suite name #TODO
                    
                    if isDebugMode:
                        print("ERROR: Suite contains no ID attribute")
                else:
                    parentSuiteNameShort = "???" #TODO
                    
                #process per-suite parent long name #TODO
                parentSuiteNameLong = "???"
                #process per-suite pass count
                suitePass = -1
                #process per-suite fail count
                suiteFail = -1
                #process per-suite critical test pass count #TODO
                suiteCriticalTestPassCount = -1
                #process per-suite critical test fail count #TODO
                suiteCriticalTestFailCount = -1
                #process per-suite duration
                suiteDuration = "???"
                
                #process per-suite metadata (suiteLongName, suiteStartTime, suiteEndTime, suiteElapsedTime)
                suiteLongName = "???"
                suiteStartTime = "???"
                suiteEndTime = "???"
                suiteElapsedTime = "???"
                suiteSource = "???"
                try:
                    suiteMetadata = suite.find_element_by_class_name("metadata")
                except:
                    suiteMetadata = None
                    if isDebugMode:
                        print("ERROR: suite " + suiteNameShort + " contains no metadata (start time, end time, elapsed time, long name, suite source)")
                if suiteMetadata != None:
                    suiteMetadataEntries = suiteMetadata.find_elements_by_tag_name("td")
                    if len(suiteMetadataEntries) >= 4:
                        suiteLongName = suiteMetadataEntries[0].text
                        suiteSource = suiteMetadataEntries[1].text
                        suiteTimeCodes = suiteMetadataEntries[2].text.split(" / ")
                        if len(suiteTimeCodes) >= 3:
                            suiteStartTime = suiteTimeCodes[0]
                            suiteEndTime = suiteTimeCodes[1]
                            suiteElapsedTime = suiteTimeCodes[2]
                        else:
                            suiteStartTime = "unknown"
                            suiteEndTime = "unknown"
                            suiteElapsedTime = "unknown"
                    else:
                        if isDebugMode:
                            print("ERROR: suite " + suiteNameShort + " metadata was formed unexpectedly")
                        suiteLongName = "unknown"
                        suiteStartTime = "unknown"
                        suiteEndTime = "unknown"
                        suiteElapsedTime = "unknown"
                        suiteSource = "unknown"
                    
                #process per-suite keyword(s) #TODO
                suiteKeywords = []
                
                #save per-suite details
                suite_dict["suite_name_short"] = suiteNameShort
                suite_dict["suite_name_long"] = suiteLongName #TODO
                suite_dict["parent_suite_name_short"] = parentSuiteNameShort
                suite_dict["parent_suite_name_long"] = parentSuiteNameLong
                suite_dict["suite_pass_count"] = suitePass #TODO
                suite_dict["suite_fail_count"] = suiteFail #TODO
                suite_dict["suite_crit_pass_count"] = suiteCriticalTestPassCount #TODO
                suite_dict["suite_crit_fail_count"] = suiteCriticalTestFailCount #TODO
                suite_dict["suite_duration"] = suiteDuration
                suite_dict["suite_source"] = suiteSource
                suite_dict["suite_start_time"] = suiteStartTime
                suite_dict["suite_end_time"] = suiteEndTime
                suite_dict["suite_elapsed_time"] = suiteElapsedTime
                suite_dict["suite_keywords"] = suiteKeywords #TODO
                
                per_suite_list.append(suite_dict)
                
                #process per-test details for tests which are direct children of that id

            '''
            while suitesContinue:
                while testsContinue:
                    #try to get div contents
                    elems = browser.find_elements_by_id("s" + str(suiteCounter) + "t" + str(testCounter))
                    if len(elems) == 0: 
                        testsContinue = False #if no div was found, continue to next suite
                        if testCounter == 1:#if no div and this is the first attempt, there are no more suites
                            suitesContinue = False
                    #else: #we have a corresponding test, extract its contents


                    testCounter += 1
                suiteCounter += 1
                testCounter = 1
                testsContinue = True
            '''
            
            numSuites = suiteCounter
            #===GENERATE FILE SUMMARY================================================================================
            if outputMode == 0: #if in version 0, generate the legacy report format
                report_dict["suite_name"] = firstSuiteName
                #report_dict["time"] = firstSuiteTime #consider using one of the other time metrics here
                report_dict["time"] = allGenTime #consider using one of the other time metrics here
                report_dict["critical_pass"] = numSuccessesCrit
                report_dict["critical_fail"] = numFailsCrit
                report_dict["pass"] = numSuccesses
                report_dict["fail"] = numFails
            
                reports[report_url] = report_dict
            elif outputMode == 1: #if in version 0, generate the DOM-based report format
                summary_dict["crit_pass_count"] = numSuccessesCrit
                summary_dict["crit_fail_count"] = numFailsCrit
                summary_dict["all_pass_count"] = numSuccesses
                summary_dict["all_fail_count"] = numFails
                summary_dict["crit_duration"] = critTestsTime
                summary_dict["all_duration"] = allTestsTime
                summary_dict["all_gen_time"] = allGenTime
                
                reports[report_url] = {"summary": summary_dict, "suites": per_suite_list, "tests": per_test_list}

    #clean up before returning
    browser.quit()

    return reports
    
def parse_reports_html(reports_dir, outputMode):
    return parse_reports_html_debug(reports_dir, outputMode, False)

#if running this file alone for debug purposes, you may find the below useful
'''
test_mode = 0
debug_mode = 1
test_reports = parse_reports_html_debug(os.path.realpath('./back-end/reports'), test_mode, debug_mode)
if test_mode == 0:
    print("Suite Name: " + test_reports["cats_log_20190426-093757_nuage5.3.3_vspk5.3.3.html"]["suite_name"])
    print("Time: " + test_reports["cats_log_20190426-093757_nuage5.3.3_vspk5.3.3.html"]["time"])
    num_pass = test_reports["cats_log_20190426-093757_nuage5.3.3_vspk5.3.3.html"]["pass"]
    num_fail = test_reports["cats_log_20190426-093757_nuage5.3.3_vspk5.3.3.html"]["fail"]
    total = num_pass + num_fail
    print("Num passed: " + str(num_pass) + " Num failed: " + str(num_fail) + " Percent passed: " + str(num_pass*100/total))
#elif test_mode = 1

print()

print(str(test_reports))
'''