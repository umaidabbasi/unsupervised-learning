# Unsupervised Learning

Algorithmic dashboards

## Hosted on NESC
    
   Front End: http://10.131.73.68/
   Back End API: http://10.131.73.68:443/users

## Required software
### Manual install
    firefox - Firefox is a freely-available web browser used by selenium to access Robot reports
    
### Automatically Installed
    geckodriver - WebDriver for Firefox, used by selenium to access Robot reports
        geckodriver is installed and added to PATH upon the back end start up. it is available under the
        Mozilla Public License from https://github.com/mozilla/geckodriver

## Start up
### Front end

    The Front must be built using:
    cd unsupervised-learning/front-end/
    npm build
    npm start

### Back End
    
    The Backend of this APP is written is python, using Flask as a web server. This allows the back end of the app to have rest interface
    that the front end can consume. 
    Flask:http://flask.pocoo.org/

    Before starting up the back end, make sure the reports directory has been created and the robot html files are present. 
    Path: unsupervised-learning/back-end/reports

    python back-end/flask_app.py
    http://127.0.0.1:5000/ 
    
    By default the app will start on the localhost.
    
    The Backend Auto generates the user.csv now, With 50 users and all the reports in the back-end/reports dir. This can
    take serveral minutes on boot, depending on the number of reports to be loaded. 
    
## Weighted Approximate-Rank Pairwise (WARP) Loss Model
    Report recommendations are made to the end-user based on a machine learning algorithm that compares user 
    interactions (viewing reports) with the past actions of other users. This returns a ranked list of reports based on
    what similar users have viewed (or not viewed). Once the model has been generated, report recommendations can be 
    made very quickly which is important for a reponsive web application. 
    
    The LightFM library is used to implement the WARP loss model, and the Pandas data analysis library 
    is used for matrix manipulation:
    
    LightFM: (http://lyst.github.io/lightfm/docs/home.html)
    Pandas: (https://pandas.pydata.org/)
    WARP: (http://www.cs.cornell.edu/~ylongqi/paper/HsiehYCLBE17.pdf)
    geckodriver (https://developer.mozilla.org/en-US/docs/Web/WebDriver)
